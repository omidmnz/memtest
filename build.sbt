name := "akka-quickstart-scala"

version := "1.0"

scalaVersion := "2.12.6"

cancelable in Global := true

lazy val akkaVersion = "2.5.21"

libraryDependencies ++= Seq(
  "com.typesafe.akka" %% "akka-actor" % akkaVersion,
)

javaOptions in run += Seq(
  "-Djava.rmi.server.hostname=127.0.0.1",
  "-Dcom.sun.management.jmxremote.port=9186",
  "-Dcom.sun.management.jmxremote.rmi.port=9186",
  "-Dcom.sun.management.jmxremote.ssl=false",
  "-Dcom.sun.management.jmxremote.local.only=false",
  "-Dcom.sun.management.jmxremote.authenticate=false",
  "-J-agentlib:jdwp=transport=dt_socket,server=y,suspend=n,address=5005"
).mkString(" ")
