package com.miras.miniature

import akka.actor.{Actor, ActorSystem, Props}

case class Test(t: Long)
case class Delay(t: Long)
case class KickStart(t: Long)

class CActor extends Actor {
  var delayCounter = 0
  var buffer = List[Test]()
  override def receive = {
    case Test(t) =>
      {
        println(s"In Actor: $t")
        self ! Delay(t)
      }
    case Delay(t) =>
      delayCounter += 1
      if (delayCounter > 1000000) {
        delayCounter = 0
      }
      self ! KickStart(t)
    case KickStart(t) =>
      if (t >= 9999999) {
        for (i <- 1 until 10000000) {
          println(s"Writing for the second time: $i")
          self ! Test(i)
        }
      }
  }
}

object Main extends App {
  val system = ActorSystem()
  val cActor = system.actorOf(Props[CActor])
  for (i <- 1 until 10000000) {
    println(s"Writing to Queue: $i")
    cActor ! Test(i)
  }
}
